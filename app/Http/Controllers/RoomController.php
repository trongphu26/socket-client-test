<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\ChatParticipants;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    public function getById(Request $request, $room_id)
    {
        $room = Room::where('room.id', $room_id)
            ->with('chatParticipants.user')
            ->first();

        $chat_name = '';

        foreach ($room->chatParticipants as $participant) {
            if ($participant->user_id != auth()->id()) {
                if ($room->conversation_type == Room::PAIR) {
                    $chat_name = $participant->user->name;
                } else {
                    $chat_name .= $participant->user->name . ', ';
                }
            }
        }
        $room->chat_name = $chat_name;

        return response()->json($room);
    }

    public function getRoom(Request $request)
    {
        $list_room_id = Room::join('chat_participants', 'chat_participants.room_id', '=', 'room.id')
            ->where('chat_participants.user_id', auth()->id())
            ->pluck('room.id')->toArray();

        $rooms = Room::whereIn('id', $list_room_id)
            ->with('chatParticipants.user')
            ->get();

        foreach ($rooms as $room) {
            $chat_name = '';
            foreach ($room->chatParticipants as $participant) {
                if ($participant->user_id != auth()->id()) {
                    if ($room->conversation_type == Room::PAIR) {
                        $chat_name = $participant->user->name;
                    } else {
                        $chat_name .= $participant->user->name . ', ';
                    }
                }
            }
            $room->chat_name = $chat_name;
        }

        return response()->json($rooms);
    }

    public function createRoom(Request $request)
    {
        $guest_email = $request->input('guest');

        $guest_user = User::where('email', $guest_email)->first();

        if (!$guest_user) {
            return response()->json(['code' => 400, 'message' => 'user not exist!']);
        }

        $check = ChatParticipants::join('room', 'chat_participants.room_id', '=', 'room.id')
            ->where('user_id', $guest_user->id)
            ->where('room.conversation_type', Room::PAIR)
            ->whereRaw('room_id in (select room_id from chat_be.chat_participants where user_id = ?)', [auth()->id()])
            ->get()->toArray();

        if (count($check) >= 1) {
            return response()->json(['code' => 400, 'message' => 'conversation exist!']);
        }

        $curTimeMillisecond = round(microtime(true) * 1000);
        $thread_id = sha1(rand(3, 5)) . $curTimeMillisecond;

        $room = new Room();
        $room->thread_id = $thread_id;
        $room->subject = 'New conversation';
        $room->admin = auth()->id();
        $room->conversation_type = Room::PAIR;
        $room->save();

        /**
         * Add participants to conversation
         */

        DB::table('chat_participants')->insert([
            ['room_id' => $room->id, 'user_id' => auth()->id(), 'status' => ChatParticipants::JOINED, 'created_at' => now(), 'updated_at' => now()],
            ['room_id' => $room->id, 'user_id' => $guest_user->id, 'status' => ChatParticipants::WAITING, 'created_at' => now(), 'updated_at' => now()],
        ]);

        return response()->json(['code' => 200, 'message' => 'ok']);
    }

    public function addUser(Request $request)
    {
        $guest_email = $request->input('guest');

        $guest_user = User::where('email', $guest_email)->first();

        if (!$guest_user) {
            return response()->json(['code' => 400, 'message' => 'user not exist!']);
        }

        $room = Room::where('room.id', $request->input('room'))->with('chatParticipants.user')->first();

        if ($room->conversation_type == Room::PAIR) {
            $anotherUser = $this->getAnotherUser(auth()->id(), $room->chatParticipants);

            $curTimeMillisecond = round(microtime(true) * 1000);
            $thread_id = sha1(rand(3, 5)) . $curTimeMillisecond;

            $room = new Room();
            $room->thread_id = $thread_id;
            $room->subject = 'New group conversation';
            $room->admin = auth()->id();
            $room->conversation_type = Room::GROUP;
            $room->save();

            DB::table('chat_participants')->insert([
                ['room_id' => $room->id, 'user_id' => auth()->id(), 'status' => ChatParticipants::JOINED, 'created_at' => now(), 'updated_at' => now()],
                ['room_id' => $room->id, 'user_id' => $guest_user->id, 'status' => ChatParticipants::WAITING, 'created_at' => now(), 'updated_at' => now()],
                ['room_id' => $room->id, 'user_id' => $anotherUser->user_id, 'status' => ChatParticipants::WAITING, 'created_at' => now(), 'updated_at' => now()],
            ]);
        } else {
            DB::table('chat_participants')->insert([
                ['room_id' => $room->id, 'user_id' => $guest_user->id, 'status' => ChatParticipants::WAITING, 'created_at' => now(), 'updated_at' => now()],
            ]);
        }

        return response()->json(['code' => 200, 'message' => 'ok']);
    }

    public function getAnotherUser($root_id, $participants)
    {
        foreach ($participants as $participant) {
            if ($root_id != $participant->user_id) {
                return $participant;
            }
        }
    }
}
