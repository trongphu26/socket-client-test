<?php

namespace App\Http\Controllers;

use App\Models\Messages;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function getMessages(Request $request, $room_id)
    {
        $messages = Messages::where('room_id', $room_id)->with('user')->get();
        return response()->json($messages);
    }
}
