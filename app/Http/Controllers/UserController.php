<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function getListUser(Request $request)
    {
        $users = User::where('id', '!=', auth()->id())->get();
        return response()->json($users);
    }

    public function getMe(Request $request)
    {
        $user = $request->user();
        return response()->json($user);
    }

    public function test(Request $request)
    {
        try {
            $user = Auth::user();

            return $request->user()->redirectToBillingPortal();
            $stripeCustomer = $user->balance();
            $taxId = $user->taxIds();

            return response()->json($taxId);

            // $transactions = $user->balanceTransactions();

            // foreach ($transactions as $transaction) {
            //     // Transaction amount...
            //     $amount = $transaction->amount(); // $2.31
            //     var_dump($amount);
            //     // Retrieve the related invoice when available...
            //     $invoice = $transaction->invoice();
            //     var_dump($invoice);
            // }

            return response()->json($transactions);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }




        // $user = Cashier::findBillable('cus_Lpdxlz8f13Lgt9');
        // return response()->json($user);
    }
}
