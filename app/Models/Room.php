<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    // conversation type
    const PAIR = 1;
    const GROUP = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room';

    public function chatParticipants()
    {
        return $this->hasMany(ChatParticipants::class, 'room_id', 'id');
    }
}
