<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Room;

class ChatParticipants extends Model
{
    use HasFactory;

    // status of user in conversation
    const WAITING = 1;
    const JOINED = 2;
    const LEAVED = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chat_participants';

    public $timestamps = true;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
