select chat_be.chat_participants.*
from chat_be.chat_participants join chat_be.room
on chat_participants.room_id = room.id
where chat_be.chat_participants.user_id = 3
and room.conversation_type = 1
and room_id in (select room_id from chat_be.chat_participants where user_id = 4)
