const express = require("express");
const http = require("http");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const db = require("./socket/models");
db.sequelize.sync();

app.get('/', (req, res) => {
    res.status(200).json({ message: 'hahahaah' })
})

module.exports = app;
