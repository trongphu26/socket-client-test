const domain = window.location.origin;
// const token = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiYmMyYzU4ZTc5MzhiZTVjMGRmMDM1M2Q1MGZjYzg1ZjcwNWY1OGFhM2ZmMDQwMmVmZjc4ZjhiMzE1NzIxMDY5YzkyMmJhOGFlZDA1NzEwMDQiLCJpYXQiOjE2NTQxNjU4ODYuOTg2NDYyLCJuYmYiOjE2NTQxNjU4ODYuOTg2NDY0LCJleHAiOjE2NTQyNTIyODYuOTgyNjYyLCJzdWIiOiIxODciLCJzY29wZXMiOltdfQ.hfaUG3AyA-3nZxTvkC8SRX9C6gb3DDzBPvJH7Vs-1fcXnCV5xlK-NGk_Ijtg0j2ssUvNBTYKEjPkZSywrtvXYaMS1ydnKzO-FaG2y93YQsFwohQgcTwxhXLNDvwQfBAOyErVApnre3_BJOQl_XLOT8_m-9k-IY-T_8heZXOA6oZ0RqtZymAuxVvPcOYn1fQubr0qbQguYobAfR6ii_tsKB1QNg8kJo3vRoAGxgEV6IR5z0yuXYC-YoQmajIbwyHgBWzZL5dvg8oOnzTHOknHoRCybnX-joDzYeGelbwE0MOd7dADv9MtD_VAWGjvuy0zr0bB2IBdaNimAMcz8Cr0BpCLI545wzgIGDuV2uu6s4yHhuOH4ZNlwaQJbEQweY7cLtz2I7RR5LoB_pgk8XD62xcUAew1ciat0soeO_9P_hfH2AonFKzHokT8Z5uv7WTbPOIVEQ3ruK6bwNa57Qn-7f3z-nJWSY5xjQEh-pJCwbmXWFIkgDR05HCgdIsc-9SvoqgJnqfRaHHRmx8JIqOuavQ2rtgIgJyAoC2durAyIjy3GTYgC16eSPmhKzBMHu-A1To3AFCrXc9Ju-b2JCRSaLKMADaS5771aZyQgUnmO9ZnxIZuVVNTwC2Vl1xCGVKzpzQW8kShOkQi2js2ui2TJzXwD8xRvrNxhXTN1gztpFg';
const token = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZmQxYWY0OGY1MDc4YzgyZmJiMjczMzhmZTJlNjMzOWM4YzhmODI0Njk3OWRmNDQ1NTg3NjY1OGU0MjhhMGYzYjJmMWJmMjIwM2ZkNTE0MWUiLCJpYXQiOjE2NTY2NDQ2NTcuMTEwODY0LCJuYmYiOjE2NTY2NDQ2NTcuMTEwODY3LCJleHAiOjE2NTY3MzEwNTcuMTAzNTgxLCJzdWIiOiIxODciLCJzY29wZXMiOltdfQ.IMkaBuhTLDUg4NsMOwhvEcFOrO7cSHoH-8utjdx-p530oyZ6ZTMPAwR1C_nYT_KsI9q6XfyWobLFZWnbxhSbv91p32Y2H9BI0IUuYPZ90Aj3k7q3ammXhCWzqgn6yBAr0O7MgETzB-A0no0mM_6Ooh6J04zpBohOqTLAu4qjQva4eOyrsiXr14yKj_yHojeo2-wbE8iWXduoG0MLbHW9Xihra8Sl3FBF462xfplFo719HezraF2W7Qt6-BJ0vO1-uccYLlzxVreZ4V0YKmyvIyZdN9iTToLlDw6OiN_4snXlau5G29dH2aBdk9Fs0jMmXZ0th2BnP2-mXQohydmhKwE57tWtZf_zjeiW5hOEgL8BQReQ27DU4lOoSbjWvSLVRP17YKoYQsqUZRA0pH5dmv8XL47GnhCcV2NOZO8TktI3C6v6ELHf5lB8WngW5eTE2hWwTQZKof0kRWKO-gCM4Px-9U4CaNadpzA3cyYANqc0nYd_oMs12xNSNbYl_QQJUWqQjXzj1UJTR9bpWhy2XVPGv1nBf-2KCSzWhNiGrptG4yezq8anPJKx1fQpGOoChZTmfsYoVfYSqaCPSDWG0WkdL60QZXhY-XZ80QSR6tdsZHok3tAgfegtmnX4ZBtVDQz-tev1FhEE3f3Q9XLqEiMjM0dMVThPSlWkgagZbtI';
$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let socket = io("http://127.0.0.1:5000", {
    // const socket = io("https://dev-pre-chat.bgs.sotatek.works/", {
        transports: ["websocket"],
        auth: {
            Authorization: token
        },
        query: {
            "user_id": 187
        }
    });

    var me;

    getMe().then(data => {
        me = data
    })
        .then(() => {
            axios({
                method: 'get',
                url: domain + '/api/room',
                responseType: 'json'
            })
            .then(function(response) {
                console.log('aac', response)
                let first_room = response.data[0]
                for (let i of response.data) {
                    $('#channel').append(`
                        <li class="clearfix channel-item">
                            <img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="avatar" />
                            <div class="about">
                                <div class="name">${i.chat_name}</div>
                                <div class="status"> <i class="fa fa-circle online"></i> online </div>
                                <div class="id" style="display:none">${i.id}</div>
                            </div>
                        </li>
                    `)
                }

                $('#send-mess').attr('room-id', first_room.id)

                return first_room
            })
            .then(getRoomMessage)
            .then(data => {
                for (let i of data) {
                    if (i.user.id == me.id) {
                        $('#messages').append(`
                                <li class="clearfix">
                                    <div class="message-data text-right">
                                        <span class="message-data-time">me</span>
                                    </div>
                                    <div class="message other-message float-right">${i.text}</div>
                                </li>
                            `);
                    } else {
                        $('#messages').append(`
                                <li class="clearfix">
                                    <div class="message-data">
                                        <span class="message-data-time">${i.user.name}</span>
                                    </div>
                                    <div class="message my-message">${i.text}</div>
                                </li>
                            `);
                    }
                }
            })
        })


        // socket.emit("join_room", 2);
    // var messages = document.getElementById('messages');
    // var form = document.getElementById('form');
    var input = document.getElementById('input');

    $('body').on('change', '#input-file', function (event) {
        console.log('hhhaha');
        console.log(document.getElementById('input-file').files);
        var file = document.getElementById('input-file').files[0];
        var reader = new FileReader();
            reader.onload = function () {
                console.log(reader);
            }
            reader.readAsDataURL(event.target.files[0]);
    });

    $('body').on('click', '#send-mess', function(e) {
        room = $(this).attr('room-id');

        if (input.value) {
            const messageData = {
                group_id: 1,
                content: input.value,
                user_id: 2,
                type: 'text',
            };
            socket.emit('send_message', messageData);
            if (messageData.author_name == me.name) {
                name = 'me'
            } else {
                name = messageData.author_name
            }
            $('#messages').append(`
                <li class="clearfix">
                    <div class="message-data text-right">
                        <span class="message-data-time">${name}</span>
                    </div>
                    <div class="message other-message float-right">${messageData.message}</div>
                </li>
            `);
            input.value = '';
            window.scrollTo(0, document.body.scrollHeight);
        } else {
            // const read = {
            //     group_id: 867,
            //     user_id: 403
            // }
            // socket.emit('read', read

            const answer = {
                event_id: 1,
                user_id_call: 187,
                user_id_answer: 3,
                status: 'accept'
            }

            socket.emit('answer', answer)
        }
    });


    socket.on('on_calling', function(data) {
        console.log('listen to event on_calling');
        console.log(data);
    });
    socket.on('on_answering', function(data) {
        console.log('listen to event on_answering');
        console.log(data);
    });


    // socket.on('receive_message', async function(data) {
    //     // console.log(data)
    //     if (data.author_name == me.name) {
    //         name = 'me'
    //     } else {
    //         name = data.author_name
    //     }
    //     $('#messages').append(`
    //         <li class="clearfix">
    //             <div class="message-data">
    //                 <span class="message-data-time">${name}</span>
    //             </div>
    //             <div class="message my-message">${data.message}</div>
    //         </li>
    //     `);
    //     window.scrollTo(0, document.body.scrollHeight);
    // });

    $('body').on('click', '#create-chat', function(event) {
        // let email = $('#email').val();

        // if (email) {
        //     axios({
        //         method: 'post',
        //         url: domain + '/api/room',
        //         responseType: 'json',
        //         data: {
        //             guest: email,
        //         }
        //     })
        //     .then(function(response) {
        //         console.log(response)
        //         if (response.data.code == 200) {
        //             toastr.success(response.data.message)
        //         } else {
        //             toastr.error(response.data.message)
        //         }
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
        // }

        socket.emit("join_room", 866);
        socket.emit("join_room", 'user.3');
        socket.emit("join_room", 'user.187');
        // socket.emit("join_room", 'user.1');
        console.log('join-room');
    });

    $('body').on('click', '#add-user', function(event) {
        // let email = $('#email-2').val();
        // let room_id = $('#room-id').val();
        // console.log(email, room_id);
        // console.log('gggg');

        // if (email) {
        //     axios({
        //         method: 'post',
        //         url: domain + '/api/room/add-user',
        //         responseType: 'json',
        //         data: {
        //             guest: email,
        //             room: room_id,
        //         }
        //     })
        //     .then(function(response) {
        //         console.log(response)
        //         if (response.data.code == 200) {
        //             toastr.success(response.data.message)
        //         } else {
        //             toastr.error(response.data.message)
        //         }
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
        // }
    });

    $('body').on('click', '.channel-item', function(event) {
        $('.channel-item').each(function(index, element) {
            $(element).removeAttr("style");
        })
        $(this).attr("style", "background-color: #cce5ffba;");

        let room_id = $(this).find('div.about').find('div.id').text();
        getRoom(room_id).then(data => {
            getRoomMessage(data).then(data => {
                $('#messages').empty();
                for (let i of data) {
                    if (i.user.id == me.id) {
                        $('#messages').append(`
                                <li class="clearfix">
                                    <div class="message-data text-right">
                                        <span class="message-data-time">me</span>
                                    </div>
                                    <div class="message other-message float-right">${i.text}</div>
                                </li>
                            `);
                    } else {
                        $('#messages').append(`
                                <li class="clearfix">
                                    <div class="message-data">
                                        <span class="message-data-time">${i.user.name}</span>
                                    </div>
                                    <div class="message my-message">${i.text}</div>
                                </li>
                            `);
                    }
                }
            })
        }).then(() => {
            socket.emit("join_room", room_id);
            $('#send-mess').attr('room-id', room_id)
        })
    });
});

function getMe() {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: domain + '/api/me',
            responseType: 'json'
        })
            .then(function (response) {
                console.log(response)
                resolve(response.data)
            })
            .catch(error => {
                reject(error);
            })
    })
}

function getRoomMessage(first_room) {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: domain + '/api/room/' + first_room.id + '/messages',
            responseType: 'json'
        })
            .then(function (response) {
                $('.chat-about').html(first_room.chat_name)
                $('#room-id').val(first_room.id)
                resolve(response.data)
            })
            .catch(error => {
                reject(error);
            })
    })
}

function getRoom(room_id) {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: domain + '/api/room/' + room_id,
            responseType: 'json'
        })
            .then(function (response) {
                resolve(response.data)
            })
            .catch(error => {
                reject(error);
            })
    })
}
