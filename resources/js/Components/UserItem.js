import React from 'react';
import { Link, Head } from '@inertiajs/inertia-react';

export default function UserItem(props) {
    return (
        <li class="clearfix">
            <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="avatar" />
            <div class="about">
                <div class="name">${props.username}</div>
                <div class="status"> <i class="fa fa-circle offline"></i> left 7 mins ago </div>
            </div>
        </li>
    );
}
