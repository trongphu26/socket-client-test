const { DataTypes } = require('sequelize');

module.exports = (sequelize, Sequelize) => {
    const Message = sequelize.define("messages", {
        room_id: {
            type: DataTypes.BIGINT
        },
        user_id: {
            type: DataTypes.BIGINT
        },
        text: {
            type: DataTypes.STRING
        },
        timestamp: {
            type: DataTypes.STRING
        }
    }, {
        sequelize,
        timestamps: true,
        underscored: true
    });

    return Message;
};
