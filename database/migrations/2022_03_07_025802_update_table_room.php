<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->string('timestamp')->change();;
            $table->bigInteger('room_id')->change();;
            $table->bigInteger('user_id')->change();;
        });

        Schema::table('chat_participants', function (Blueprint $table) {
            $table->bigInteger('room_id')->change();;
            $table->bigInteger('user_id')->change();;
        });

        Schema::table('room', function (Blueprint $table) {
            $table->bigInteger('admin');
        });

        Schema::table('chat_participants', function (Blueprint $table) {
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
