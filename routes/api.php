<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

Route::post('/login', [AuthController::class, 'login']);
Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/users', 'UserController@getListUser');
    Route::get('/me', 'UserController@getMe');
    Route::get('/room/{room_id}/messages', 'MessagesController@getMessages');
    Route::get('/room', 'RoomController@getRoom');
    Route::post('/room', 'RoomController@createRoom');
    Route::post('/room/add-user', 'RoomController@addUser');
    Route::get('/room/{room_id}', 'RoomController@getById');

    Route::get('/stripe/test', [UserController::class, 'test']);
    Route::get('/billing-portal', function (Request $request) {
        return $request->user()->redirectToBillingPortal();
    });
});


